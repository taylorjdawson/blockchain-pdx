## Blockchain PDX Code of Conduct

The Blockchain PDX meetup group is dedicated to providing a harassment-free community for everyone, regardless of sex, gender identity or expression, sexual orientation, disability, physical appearance, age, body size, race, nationality, or religious beliefs. We do not tolerate harassment of community members in any form. Participants violating these rules may be sanctioned or expelled from the community at the discretion of the Blockchain PDX organizers.

Harassment includes offensive verbal or written comments related to sex, gender identity or expression, sexual orientation, disability, physical appearance, age, body size, race, nationality, or religious beliefs, deliberate intimidation, threats, stalking, following, harassing photography or recording, sustained disruption of talks or other events, inappropriate physical contact, and unwelcome sexual attention. Sexual language and imagery is not appropriate for any Blockchain PDX event or communication channel. Community members asked to stop any harassing behavior are expected to comply immediately. Sponsors and presenters are also subject to the anti-harassment policy.

If a community member engages in harassing behavior, the Blockchain PDX organizers may take any action they deem appropriate, including warning the offender or expulsion from the Blockchain PDX community. If you are being harassed, notice that someone else is being harassed, or have any other concerns, please contact a Blockchain PDX organizer immediately.

See [Blockchain PDX meetup website](https://www.meetup.com/Blockchain-PDX/)

If you have questions or feedback about this Code of Conduct please contact one of the organizers.

Blockchain PDX organizers developed this Code of Conduct to govern all Blockchain PDX events and communication channels. We used the [Portland Python User Group Code of Conduct](https://www.meetup.com/pdxpython/pages/12061872/Code_of_Conduct/) as a starting point. This Code of Conduct, like its inspiration, is licensed under the [Creative Commons Zero license](https://creativecommons.org/publicdomain/zero/1.0/).
