![Blockchain PDX](./images/meetup_background_2.png)

# Blockchain PDX

A meetup for people who are interested in the technical side of Ethereum & other blockchains. From developers to entrepreneurs, our community members have access to a global network of innovators, business thought leaders, and blockchain enthusiasts.

Members attending agree to the [Code Of Conduct](./Code_Of_Conduct.md).

### Past Meetups:

* [BUIDL an Ethereum dApp](./slides/Buidl_Dapp_profile.md) - Portfolio Contract - Basics of Embark, Amberdata.io, solidity & ethereum! May 29th, 2019
* [BUIDL an Ethereum dApp - Part 2](./slides/Buidl_Dapp_directory.md) - Directory Contract - Basics of Remix, Amberdata.io, solidity & ethereum! June 26th, 2019, See [live meetup notes here](https://hackmd.io/@trevor/buidldapp-part2)
* [Intro to Blockchain](./slides/Intro-to-Blockchain) - Overview and history of the blockchain. 

### dApp Examples:

* [Portfolio Card](https://gitlab.com/TrevorJTClarke/profile_vue) - The example for BUIDL an Ethereum dApp, May 29th, 2019
* [Directory + Portfolio](https://github.com/TrevorJTClarke/Circles) - The example "Circles" dApp with directory and portfolio contracts, June 26th, 2019
