---
title: Intro to Blockchain
tags: Blockchain, PDX, Talk
description: Slides for the meetup Blockchain PDX, buidling a dapp on Ethereum! View the slides with "Slide Mode".
---

## Welcome!

###### Snacks & drinks are available, please help yourself!

![](https://i.imgur.com/W33qsVj.png)

<small>Join our slack group!</small>

<small>

* Wifi: "Blockchain Meetup", Password: "learnblockchain"
* Code of Conduct: https://gitlab.com/TrevorJTClarke/blockchain-pdx/blob/master/Code_Of_Conduct.md

</small>

<small>Thank you to our sponsors: [Amberdata.io](https://amberdata.io), [Consensys](https://www.buidlnetwork.net/), [10net](10net.net)</small>

---

## Intro to Blockchain

- What is the "blockchain"?
- The History surrounding blockchain
- What problems does it solve
- (Most importantly) How can I use it!

---

### In a sentence...

A blockchain is a <span>decentralized<!-- .element: class="fragment highlight-red" data-fragment-index="0" --></span>, <span>distributed<!-- .element: class="fragment highlight-red" data-fragment-index="1" --></span>, and oftentimes <span>public<!-- .element: class="fragment highlight-red" data-fragment-index="2" --></span>, <span>digital<!-- .element: class="fragment highlight-red" data-fragment-index="3" --></span> <span>ledger<!-- .element: class="fragment highlight-red" data-fragment-index="4" --></span> that is used to record transactions across many computers so that any involved record <span>cannot be altered<!-- .element: class="fragment highlight-red" data-fragment-index="5" --></span> retroactively, without the alteration of all <span>subsequent blocks<!-- .element: class="fragment highlight-red" data-fragment-index="6" --></span>.

-- [Wikipedia](https://en.wikipedia.org/wiki/Blockchain)


<div style="position: relative; width: 100%; height: 100%">

<span style="
    position: absolute;
    left: 50%;
    transform: translate(-50%, 50%);
    top: 0;
    width: 100%
">not one single entity has control<!-- .element: class="fragment fade-in-then-out" data-fragment-index="0" --></span>

<span style="
    position: absolute;
    left: 50%;
    transform: translate(-50%, 50%);
    top: 0;
    width: 100%
" >the processing doesn't happen in the same place<!-- .element: class="fragment fade-in-then-out" data-fragment-index="1" --></span>

<span style="
    position: absolute;
    left: 50%;
    transform: translate(-50%, 50%);
    top: 0;
    width: 100%
" >anyone can view records/transactions - transparency!<!-- .element: class="fragment fade-in-then-out" data-fragment-index="2" --></span>

<span style="
    position: absolute;
    left: 50%;
    transform: translate(-50%, 50%);
    top: 0;
    width: 100%
" >not physical - on the internet<!-- .element: class="fragment fade-in-then-out" data-fragment-index="3" --></span>


<span style="
    position: absolute;
    left: 50%;
    transform: translate(-50%, 50%);
    top: 0;
    width: 100%
" >a record of entries e.g. transactions<!-- .element: class="fragment fade-in-then-out" data-fragment-index="4" --></span>

<span style="
    position: absolute;
    left: 50%;
    transform: translate(-50%, 50%);
    top: 0;
    width: 100%
" >immutability! <!-- .element: class="fragment fade-in-then-out" data-fragment-index="5" --></span>



<span style="
    position: absolute;
    left: 50%;
    transform: translate(-50%, 50%);
    top: 0;
    width: 100%
    " > forms a chain! <!-- .element: class="fragment fade-in-then-out" data-fragment-index="6" --></span>

</div>


---

![](https://i.imgur.com/k18XMTn.png)


---

# The History

---

## Surety's Absolute Proof (1991)

<div style="position: relative; text-align: left; padding-left: 25px">

Creators: Haber and Stornetta
Nutshell: a "hash-chain" of timestamped digital document signatures that are published on the New York Times weekly. Enables customers to verify the timestamp and integrity of a digital document.

<small>(Cited 3 times in the Bitcoin whitepaper)</small>


</div>



---

## Surety's Absolute Proof (1991)

![](https://i.imgur.com/uVnGa4m.png)

---


## Surety's Absolute Proof (1991)

<div style="position: relative; text-align: left; padding-left: 25px">


Contributing concepts: hash-chain, distibuted ledger system

</div>



---

## Surety's Absolute Proof (1991)

### Is it "Hackable" ??

![](https://i.imgur.com/njkxiQi.png)


---

## Hashcash (1997)

<div style="position: relative; text-align: left; padding-left: 25px">


Creator: Adam back

Nutshell: A cryptographic hash-based proof-of-work algorithm used to limit email spam and denial-of-service attacks. Limits spam by requiring the sender to spend CPU time to create a hash the the receiver can quickly verify.

Contributing concept: Proof-of-Work

</div>

---

## B-money (1998)

<div style="position: relative; text-align: left; padding-left: 25px">

Creator: Wei Dai
Nutshell: an "anonymous, distributed electronic cash system"
Contributing concepts: Anonymity, secure electronic transactions, in-network contract enforcement (primative Ethreum Smart Contracts)

</div>

---

## Bitgold (2005)

<div style="position: relative; text-align: left; padding-left: 25px">

Creator: Nick Szabo
Nutshell: "a financial system that combines different elements of cryptography and mining to accomplish decentralization" *
Contributing concept: Proof of Work, decentralization

</div>

<small>* [Investopedia](https://www.investopedia.com/terms/b/bit-gold.asp)</small>


---

## Bitcoin (2009)

<div style="position: relative; text-align: left; padding-left: 25px">

Creator: Satoshi Nakamoto
Nutshell: Is a decentralized digital currency without a central bank or single administrator that can be sent from user to user on the peer-to-peer bitcoin network without the need for intermediaries.
Contributing concept: The first fully operational blockchain. 

</div>

---

## Ethereum (2016)

<div style="position: relative; text-align: left; padding-left: 25px">

Co-founders: Vitalik Buterin, Gavin Wood, Joseph Lubin
Nutshell: Ethereum is an open source, public, blockchain-based distributed computing platform and operating system featuring smart contract functionality.
Contributing concept: Smart Contracts

</div>

---

## Smart Contract
<small>
Immutable self-executing, code that lives on the blockchain and allows for trustless transactions and agreements amongst disparate, anonymous parties without the need for a central authority, legal system, or external enforcement mechanism
</small>

<span> Ex. Tamper-proof Vending Machine <!-- .element: class="fragment fade-in" data-fragment-index="0" --></span>

---

## Today

![](https://i.imgur.com/3alAPyw.png)

---

## What do these projects have in common?

<span> They need a way to easily extract meaningful data from the Blockchain.<!-- .element: class="fragment fade-in" data-fragment-index="0" --></span>

---

## How: The hard way

<div style="position: relative; text-align: left; padding-left: 25px">
Run a server that talks to the network</br>

Extracting data via Json RPC</br>

Aggregating data</br>

</div>

---

## How: The easy way
Using Amberdata!

#### We do the heavy lifting 💪

<div style="position: relative; text-align: left; padding-left: 25px">

Running nodes & extracting data

Storing data that is optimized for advanced queries

Providing historical data

Interweaving Market Data with block chain data

</div>

---

## The API

- Over 100 endpoints
- Real-time & historical time series Blockchain & Market data
- JSON RPC Compliant methods

[Documentaion ➞](https://docs.amberdata.io/reference)

---

## The API Key

Onboarding is fast!

[Getting Started ➞](https://amberdata.io/onboarding)

---

### Example: Get transaction + Market Data

[Example ➞](https://runkit.com/taylorjdawson/web3data-js-get-transaction)

---

### Example: Token Rankings

[Example ➞](https://runkit.com/taylorjdawson/web3data-js-example-token-rankings)

---

### Example: Real-time Data - Blocks

[Example ➞](https://runkit.com/taylorjdawson/web3data-js-example-websocket)

---

## Real life examples:

- Block viz
- NFTs

---

Thanks for listening!

Slides available [here](https://hackmd.io/@1XqK8hlWRW6FMt8Q5EAXJw/ryY2tggwB).

Website: Amberdata.io
Docs: Docs.amberdata.io
Follow us on [Medium](https://medium.com/amberdata) & [Twitter](https://twitter.com/Amberdataio)

---
